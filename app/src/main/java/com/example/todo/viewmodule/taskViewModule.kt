package com.example.todo.viewmodule

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.todo.database.entity.TaskEntity
import com.example.todo.database.repository.TaskLocalDataRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TaskViewModel(private val taskLocalDataRepository: TaskLocalDataRepository) : ViewModel() {

    private var _taskList = MutableLiveData<List<TaskEntity>>()

    val taskList: LiveData<List<TaskEntity>> = _taskList

    fun getTaskList() {
         CoroutineScope(Dispatchers.IO).launch {
            val response = taskLocalDataRepository.getAllTask()
             withContext(context = Dispatchers.Main){
                 _taskList.value = response
             }
        }
    }
}