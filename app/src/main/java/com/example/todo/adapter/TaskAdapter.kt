package com.example.todo.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.todo.R
import com.example.todo.database.entity.TaskEntity
import java.util.*
import kotlin.collections.ArrayList

class TaskAdapter(
    private var taskList: List<TaskEntity>,
    private val imageCallback: (imgUrl: TaskEntity?) -> Unit
) : RecyclerView.Adapter<TaskAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.task_row, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return taskList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(taskList[position])
    }

    fun setTaskList(taskList: ArrayList<TaskEntity>) {
        this.taskList = taskList
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val tvTaskName: TextView = itemView.findViewById(R.id.tv_task_name)
        private val tvDueDate: TextView = itemView.findViewById(R.id.tv_due_date)
        private val ivTaskStatus: ImageView = itemView.findViewById(R.id.iv_task_icon)
        private val ivTaskOption: ImageView = itemView.findViewById(R.id.iv_task_option)
        private val taskView: View? = itemView

        fun bind(taskEntity: TaskEntity) {
            tvTaskName.text = taskEntity.taskTitle
            val calendar = Calendar.getInstance()
//            calendar.timeInMillis = taskEntity.createdDate.toLong()
            tvDueDate.text = String.format(
                Locale.US,
                "%d/%d/%d",
                calendar[Calendar.DATE],
                calendar[Calendar.MONTH] + 1,
                calendar[Calendar.YEAR]
            )

/*            ivTaskOption.setOnClickListener { v ->
                val popup = PopupMenu((listener as Context?)!!, v)
                popup.setOnMenuItemClickListener(this@TaskAdapterView)
                popup.inflate(R.menu.task_menu_option)
                popup.show()
            }
            taskView!!.setOnClickListener { listener.onTaskClick(taskEntity) }*/
        }
    }
}