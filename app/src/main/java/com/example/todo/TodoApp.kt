package com.example.todo

import android.app.Application
import com.example.todo.module.dbModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.module.Module

class TodoApp: Application() {
    override fun onCreate() {
        super.onCreate()
        val appModules: MutableList<Module> = ArrayList()
        appModules.add(dbModule)
        startKoin {
            androidContext(this@TodoApp)
            modules(appModules)
        }
    }
}