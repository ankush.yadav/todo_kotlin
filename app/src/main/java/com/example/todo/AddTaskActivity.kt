package com.example.todo

import android.app.Activity
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.app.TimePickerDialog
import android.app.TimePickerDialog.OnTimeSetListener
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.todo.database.AppDatabase
import com.example.todo.database.entity.TaskEntity
import com.example.todo.util.Common
import kotlinx.android.synthetic.main.activity_add_task.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import java.util.*

class AddTaskActivity : AppCompatActivity() {

    private var alarmYear = 0
    private var alarmMonth = 0
    private var alarmDay = 0

    private val db: AppDatabase by inject()
    private lateinit var task: TaskEntity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_task)
        setListener()
    }


    private fun setListener() {
        val setDateCalendar = Calendar.getInstance()
        bt_set_due_date.setOnClickListener { // Get Current Date
            val c = Calendar.getInstance()
            val mYear = c[Calendar.YEAR]
            val mMonth = c[Calendar.MONTH]
            val mDay = c[Calendar.DAY_OF_MONTH]
            val datePickerDialog = DatePickerDialog(
                this@AddTaskActivity,
                OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                    setDate(year, monthOfYear, dayOfMonth)
                    setDateCalendar!!.set(Calendar.DATE, dayOfMonth)
                    setDateCalendar!!.set(Calendar.MONTH, monthOfYear)
                    setDateCalendar!!.set(Calendar.YEAR, year)
                    tv_due_date.text = String.format(
                        Locale.US, "%d/%d/%d", dayOfMonth, monthOfYear + 1, year
                    )
                }, mYear, mMonth, mDay
            )
            datePickerDialog.show()
        }
        bt_set_due_time.setOnClickListener(View.OnClickListener {
            val cldr = Calendar.getInstance()
            val hour = cldr[Calendar.HOUR_OF_DAY]
            val minutes = cldr[Calendar.MINUTE]
            val picker = TimePickerDialog(
                this@AddTaskActivity,
                OnTimeSetListener { tp, sHour, sMinute ->
                    tv_due_time.text = "$sHour:$sMinute"
                    setDateCalendar!!.set(Calendar.HOUR, sHour)
                    setDateCalendar!!.set(Calendar.MINUTE, sMinute)
                }, hour, minutes, true
            )
            picker.show()
        })
        bt_add_task.setOnClickListener {
            task = TaskEntity(
                0, et_task_name.text.toString(),
                et_task_description.text.toString(),
                false,
                tv_due_date.text.toString()+":"+tv_due_time.text.toString(),
                setDateCalendar.timeInMillis.toString(),
                setDateCalendar.timeInMillis.toString()
            )
            val intent = Intent()
            intent.putExtra("Task", task)
            setResult(Activity.RESULT_OK, intent)
            CoroutineScope(Dispatchers.IO).launch {
                db.getUserDao().addTask(task)
                finish()
            }
        }
    }

    private fun setDate(year: Int, month: Int, date: Int) {
        alarmYear = year
        alarmMonth = month
        alarmDay = date
    }
}
