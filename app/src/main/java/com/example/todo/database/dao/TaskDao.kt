package com.example.todo.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.todo.database.entity.TaskEntity

@Dao
interface TaskDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addTask(userEntity: TaskEntity)

    @Update
    fun updateTask(userEntity: TaskEntity)

    @Query("SELECT * FROM task LIMIT 1")
    fun getTask(): TaskEntity

    @Query("SELECT * FROM task ")
    fun getAllTask(): List<TaskEntity>

    @Query("DELETE FROM task")
    fun clearTask()
}