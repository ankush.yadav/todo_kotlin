package com.example.todo.database.entity

import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "task")
data class TaskEntity(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "task_title") var taskTitle: String,
    @ColumnInfo(name = "task_description") val description: String,
    val priority: Boolean,
    @ColumnInfo(name = "due_date") var dueDate: String,
    @ColumnInfo(name = "created_date") val createdDate: String,
    @ColumnInfo(name = "modify_date") val modifyDate: String
) :Parcelable
{
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readByte() != 0.toByte(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString().toString()
    )

    constructor() : this(0,"","",false,"","","")

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(taskTitle)
        parcel.writeString(description)
        parcel.writeByte(if (priority) 1 else 0)
        parcel.writeString(dueDate)
        parcel.writeString(createdDate)
        parcel.writeString(modifyDate)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TaskEntity> {
        override fun createFromParcel(parcel: Parcel): TaskEntity {
            return TaskEntity(parcel)
        }

        override fun newArray(size: Int): Array<TaskEntity?> {
            return arrayOfNulls(size)
        }
    }
}