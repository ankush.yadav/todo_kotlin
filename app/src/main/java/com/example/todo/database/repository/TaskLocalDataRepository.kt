package com.example.todo.database.repository

import androidx.lifecycle.LiveData
import com.example.todo.database.AppDatabase
import com.example.todo.database.entity.TaskEntity

class TaskLocalDataRepository(private val appDatabase: AppDatabase) {
    fun getAllTask(): List<TaskEntity> {
        return appDatabase.getUserDao().getAllTask()
    }
}