package com.example.todo.module

import android.content.Context
import androidx.room.Room
import com.example.todo.database.AppDatabase
import com.example.todo.database.repository.TaskLocalDataRepository
import com.example.todo.viewmodule.TaskViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val dbModule = module {
    single { provideRoomDb(androidApplication()) }
    single { TaskLocalDataRepository(get()) }
    viewModel { TaskViewModel(get()) }
}

private fun provideRoomDb(context: Context): AppDatabase {
    return Room.databaseBuilder(context, AppDatabase::class.java, "task_db").build()
}