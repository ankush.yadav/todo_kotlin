package com.example.todo

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.example.todo.adapter.TaskAdapter
import com.example.todo.database.entity.TaskEntity
import com.example.todo.database.repository.TaskLocalDataRepository
import com.example.todo.util.Common.Companion.REQUEST_CODE
import com.example.todo.viewmodule.TaskViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import org.koin.androidx.scope.lifecycleScope

class MainActivity : AppCompatActivity() {

    private val taskViewModel:TaskViewModel by inject()
    private lateinit var taskAdapter: TaskAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        add_task.setOnClickListener {
            val intent = Intent(this@MainActivity, AddTaskActivity::class.java)
            startActivityForResult(intent, 0X0100)
        }

        setAdapter()
    }

    private fun setAdapter() {
        val taskListObserver = Observer<List<TaskEntity>> { taskList ->
            taskAdapter.setTaskList(taskList = taskList as ArrayList<TaskEntity>)
        }
        taskViewModel.taskList.observe(this, taskListObserver )

        taskViewModel.getTaskList()
        taskAdapter = TaskAdapter(emptyList<TaskEntity>()) { imgUrl -> imgUrl?.let { onTaskClick(it) } }
        rv_task_list.apply { adapter = taskAdapter }
    }

    private fun onTaskClick(taskEntity: TaskEntity) {
        TODO("Not yet implemented")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == REQUEST_CODE){
            if(resultCode == Activity.RESULT_OK){
                taskViewModel.getTaskList()
            }
        }

    }
}
